LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_C_INCLUDES := bionic

LOCAL_SRC_FILES := \
	rapl-read.c

LOCAL_MODULE := rapl-read

include $(BUILD_EXECUTABLE)
